/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.util.Date;

public class Chofer extends Persona
{
    private String codCoop;

    public Chofer(String codCoop, String cedula, String nombre, String direccion, String telefono, Date fechaNacimiento, String sexo) {
        super(cedula, nombre, direccion, telefono, fechaNacimiento, sexo);
        this.codCoop = codCoop;
    }


    public Chofer() {
    }

    public String getCodCoop() {
        return codCoop;
    }

    public void setCodCoop(String codCooperativa) {
        this.codCoop = codCooperativa;
    }

  
    
}
