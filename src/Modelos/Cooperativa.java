/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modelos;

import java.util.ArrayList;


public class Cooperativa {
    
    private String rif;
    private String nombre;
    private String direccion;
    private String telefono;

    public Cooperativa(String rif, String nombre, String direccion, String telefono) {
        super();
        this.rif = rif;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
       
    }

    public Cooperativa() {
    }

    public String getRif() {
        return rif;
    }

    public void setRif(String rif) {
        this.rif = rif;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
   
    
    
}
