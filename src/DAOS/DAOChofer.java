/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DAOS;
import Modelos.Chofer;
import bean.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 *
 * @author gerardo
 */
public class DAOChofer extends ConexionDAO {
    private List<Chofer> choferes = new ArrayList<Chofer>();

    public DAOChofer() {
        super();
    }

    public List<Chofer> getChoferes() {
        return choferes;
    }

    
    public void registrarChofer(Chofer chofer)
	{ 
           
		String tiraSQL ="INSERT INTO persona (cedula,nombre,direccion,telefono,fechanacimiento,sexo)"+
				"VALUES( '"+chofer.getCedula() +"','" +chofer.getNombre() + "','" +chofer.getDireccion()+ "','" + chofer.getTelefono()+
				"','"+ chofer.getFechaNacimiento()+"','" + chofer.getSexo()+"')";
		Conexion.ejecutar(tiraSQL);
                
                String tiraSQL2 ="INSERT INTO chofer (cod_coop,cedula)"+
				"VALUES( '"+chofer.getCodCoop()+"','" +chofer.getCedula() +"')";
		Conexion.ejecutar(tiraSQL2);
	}
    
    public void cargarChofer()
    {
       choferes = new ArrayList<Chofer>();
        ResultSet resultset =null;

    String tiraSQL = "SELECT chofer.cod_coop,chofer.cedula,persona.nombre,persona.direccion,persona.telefono,persona.fechanacimiento,persona.sexo FROM chofer,persona WHERE chofer.cedula=persona.cedula";
    
    resultset = Conexion.consultar(tiraSQL);
    
        try {
            while(resultset.next())
            {
                String cod_coop = resultset.getString("cod_coop");
                String cedula = resultset.getString("cedula");
                String nombre = resultset.getString("nombre");
                String direccion = resultset.getString("direccion");
                String telefono = resultset.getString("telefono");
                Date fechanacimiento = resultset.getDate("fechanacimiento");
                String sexo = resultset.getString("sexo");
                
                Chofer chofer = new Chofer(cod_coop, cedula, nombre, direccion, telefono, fechanacimiento, sexo);
                choferes.add(chofer);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public Chofer buscarChofer(String valor){
		Chofer choferes = new Chofer();
		ResultSet resultset = null;

		String tiraSQL = "SELECT chofer.cod_coop,chofer.cedula,persona.nombre,persona.direccion,persona.telefono,persona.fechanacimiento,persona.sexo FROM chofer,persona WHERE chofer.cedula='" + valor + "' AND chofer.cedula=persona.cedula";
		resultset = Conexion.consultar(tiraSQL);

		try{
			while (resultset.next()) {
				  String cod_coop = resultset.getString("cod_coop");
                String cedula = resultset.getString("cedula");
                String nombre = resultset.getString("nombre");
                String direccion = resultset.getString("direccion");
                String telefono = resultset.getString("telefono");
                Date fechanacimiento = resultset.getDate("fechanacimiento");
                String sexo = resultset.getString("sexo");

				choferes = new Chofer(cod_coop, cedula, nombre, direccion, telefono, fechanacimiento, sexo);
			}
		}
		catch(SQLException e){
			e.printStackTrace();
		}

		return choferes;
	}
    
    public void actualizarChofer(String cod_coop, String cedula, String nombre,String direccion,String telefono, Date fechanacimiento, String sexo){
		String tiraSQL = "UPDATE chofer SET cod_coop = '"+cod_coop +"' WHERE cedula='" +cedula+ "'";
		Conexion.ejecutar(tiraSQL);
                
                String tiraSQL2 = "UPDATE persona SET nombre = '"+nombre+"', direccion='"+direccion+"',telefono='"+telefono+"',fechanacimiento='"+fechanacimiento+"',sexo='"+sexo+"' WHERE cedula='" +cedula+ "'";
		Conexion.ejecutar(tiraSQL2);
                
	}
    
    public void eliminarChofer(String cedula)
    {
        String tiraSQL = "DELETE FROM chofer WHERE cedula='"+cedula+"'";
        Conexion.ejecutar(tiraSQL);
        String tiraSQL2 = "DELETE FROM persona WHERE cedula='"+cedula+"'";
        Conexion.ejecutar(tiraSQL2);
        
    }
    
    
}
