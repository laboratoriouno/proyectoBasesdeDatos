/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.  
 */
package Controladores;

import DAOS.DAOChofer;
import Modelos.Chofer;
import Vistas.VChofer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Date;


public class ControladorChofer extends KeyAdapter implements ActionListener, KeyListener 
{
    
    private VChofer vchofer;
    private DAOChofer choferDAO;

    public ControladorChofer()
    {
        super();
        vchofer = new VChofer();
        choferDAO = new DAOChofer();
        
        vchofer.agregarListener(this);
        vchofer.setLocationRelativeTo(null);
        vchofer.setVisible(true);
       
        enableBotones(false);
        
        vchofer.getBtnNuevo().setEnabled(true);
        vchofer.getBtnSalir().setEnabled(true);
        vchofer.getBtnBuscar().setEnabled(true);
        vchofer.getBtnCancelar().setEnabled(true);

        
      CargarTablaChoferes();
        
   
        vchofer.getTxtCedula().addKeyListener(new KeyAdapter()
    { 

        @Override
        public void keyTyped(KeyEvent e) 
        {     
//            if(Character.isAlphabetic(e.getKeyChar())){ //validando que NO es numero
//			e.consume(); //Desecha el caracter
//            }
            
            String Cadena=vchofer.getTxtCedula().getText();
            if (Cadena.length()==8)
            {  
                e.consume();   
                return;
            } 
            
            
        }
        
        
                  
        @Override
        public void keyPressed(KeyEvent e) 
        {
            cedulakeyPressed(e);            
        }
    }
                 
                 
    );
        
          vchofer.getTxtNombre().addKeyListener(new KeyAdapter() 
       {
           @Override
          public void keyTyped(KeyEvent e) 
             {
               		if(Character.isDigit(e.getKeyChar())){ //validando que NO es numero
			e.consume(); //Desecha el caracter
		}
             }
       }
       );
          
          vchofer.getTxtTelefono().addKeyListener(new KeyAdapter() 
       {
           @Override
          public void keyTyped(KeyEvent e) 
             {
               		if(Character.isAlphabetic(e.getKeyChar())){ //validando que NO es numero
			e.consume(); //Desecha el caracter
		}
             }
       }
       );   
    }
    
     private void cedulakeyPressed(KeyEvent e)    
{
   String Cadena;
   int posChofer;
   Chofer chofer=new Chofer();
   Cadena =vchofer.getTxtCedula().getText();
   vchofer.getTxtCedula().setText(Cadena.toUpperCase());
      
   
   if (e.getKeyChar()==10 && Cadena.length() < 10) 
       
   {
         chofer=choferDAO.buscarChofer(Cadena); 
         
         vchofer.getTxtNombre().setText(chofer.getNombre());
          vchofer.getTxtDireccion().setText(chofer.getDireccion()); 
          vchofer.getDchNacimiento().setDate(chofer.getFechaNacimiento());
          vchofer.getTxtTelefono().setText(chofer.getTelefono());
          vchofer.getCmbSexo().setSelectedItem(chofer.getSexo());
          vchofer.getTxtCooperativa().setText(chofer.getCodCoop());
          
          
        enableBotones(false);
        vchofer.getBtnActualizar().setEnabled(true);
        vchofer.getBtnBorrar().setEnabled(true);
        vchofer.getBtnCancelar().setEnabled(true);
        vchofer.getBtnSalir().setEnabled(true);
          enableTxt(true);
         vchofer.getTxtCedula().setEnabled(false);
                   
      } 
   }
 
      private void enableTxt(boolean Status)    
{
   vchofer.getTxtCedula().setEnabled(Status);
   vchofer.getTxtNombre().setEnabled(Status); 
   vchofer.getTxtDireccion().setEnabled(Status);
   vchofer.getDchNacimiento().setEnabled(Status);
   vchofer.getTxtTelefono().setEnabled(Status);
   vchofer.getCmbSexo().setEnabled(Status);
   vchofer.getTxtCooperativa().setEnabled(Status);
   
} 
    
    private void Limpiar()    
{
   vchofer.getTxtCedula().setText("");
   vchofer.getTxtNombre().setText(""); 
   vchofer.getTxtDireccion().setText("");
   vchofer.getDchNacimiento().setDate(null);
   vchofer.getTxtTelefono().setText("");
   vchofer.getCmbSexo().setSelectedIndex(0);
   vchofer.getTxtCooperativa().setText("");
}    
    
    private void enableBotones(boolean Status)
    {
        vchofer.getBtnGuardar().setEnabled(Status);
        vchofer.getBtnActualizar().setEnabled(Status);
        vchofer.getBtnBorrar().setEnabled(Status);
        vchofer.getBtnCancelar().setEnabled(Status);
        vchofer.getBtnNuevo().setEnabled(Status);
        vchofer.getBtnBuscar().setEnabled(Status);
        
    }
    
        private void CargarTablaChoferes()
   {
      
    int Fila;
    Chofer chofer=new Chofer();
    choferDAO.getChoferes().clear();
    choferDAO.cargarChofer();
    vchofer.getTabChoferes().removeAll();
    for (Fila=0;Fila<choferDAO.getChoferes().size();Fila++)
    {
       chofer=choferDAO.getChoferes().get(Fila);

       vchofer.getTabChoferes().setValueAt(chofer.getCedula(), Fila, 0);
       vchofer.getTabChoferes().setValueAt(chofer.getNombre(), Fila, 1);
       vchofer.getTabChoferes().setValueAt(chofer.getDireccion(), Fila, 2);
       vchofer.getTabChoferes().setValueAt(chofer.getTelefono(), Fila, 3);
       vchofer.getTabChoferes().setValueAt(chofer.getFechaNacimiento(), Fila, 4);
       vchofer.getTabChoferes().setValueAt(chofer.getSexo(), Fila, 5);
       vchofer.getTabChoferes().setValueAt(chofer.getCodCoop(), Fila, 6);
    }     
    

   }  
        
        public void actualizarChofer()
        {
            choferDAO = new DAOChofer();
            choferDAO.actualizarChofer(vchofer.getTxtCooperativa().getText(), vchofer.getTxtCedula().getText(),vchofer.getTxtNombre().getText(), vchofer.getTxtDireccion().getText(), vchofer.getTxtTelefono().getText(),vchofer.getDchNacimiento().getDate(), vchofer.getCmbSexo().getSelectedItem().toString());
            vchofer.mostrarMensaje("El chofer ha sido modificado exitosamente");
            Limpiar();
            enableTxt(false);
            vchofer.getTxtCedula().setEnabled(true);
            vchofer.getTxtCedula().requestFocus();
            vchofer.getBtnActualizar().setEnabled(false);
            vchofer.getBtnBorrar().setEnabled(false);
            vchofer.getBtnNuevo().setEnabled(true);
            vchofer.getBtnBuscar().setEnabled(true);
            
            
            
            CargarTablaChoferes();
        }
     public void eliminarChofer()
        {
            choferDAO = new DAOChofer();
            choferDAO.eliminarChofer(vchofer.getTxtCedula().getText());
            vchofer.mostrarMensaje("El chofer ha sido eliminado exitosamente");
            Limpiar();
            enableTxt(false);
            enableBotones(false);
            
            vchofer.getBtnBuscar().setEnabled(true);
            vchofer.getBtnNuevo().setEnabled(true);
            vchofer.getBtnSalir().setEnabled(true);
            vchofer.getBtnCancelar().setEnabled(true);
            vchofer.getTxtCedula().setEnabled(true);
            vchofer.getTxtCedula().requestFocus();
            vchofer.setTabChoferes(null);
            CargarTablaChoferes();
        }
    

    @Override
    public void actionPerformed(ActionEvent e) 
    {
        
        String cedula;
        String nombre;
        String direccion;
        String telefono;
        Date fecha;
        String sexo;
        String codCoop;
        
        choferDAO = new DAOChofer();
        
        
        
        
            if(e.getSource().equals(vchofer.getBtnNuevo()))
            {
                    
                
                enableBotones(false);
                vchofer.getBtnGuardar().setEnabled(true);
                vchofer.getBtnCancelar().setEnabled(true);
                enableTxt(true);
                Limpiar();
                vchofer.getTxtCedula().requestFocus();

            
            }
             if (e.getSource().equals(vchofer.getBtnCancelar()))
        {
                enableBotones(false);
                enableTxt(false);
                
                Limpiar();
                
                vchofer.getBtnCancelar().setEnabled(true);
                vchofer.getBtnBuscar().setEnabled(true);
                vchofer.getBtnNuevo().setEnabled(true);
                vchofer.getBtnSalir().setEnabled(true);
                
                vchofer.getTxtCedula().setEnabled(true);
                vchofer.getTxtCedula().requestFocus();
        }
            
            
            if(e.getSource().equals(vchofer.getBtnBuscar()))
            {
                   String Cadena;
                    Chofer chofer=new Chofer();
                    Cadena =vchofer.getTxtCedula().getText();
                    vchofer.getTxtCedula().setText(Cadena.toUpperCase());

                    if (Cadena.length() > 0) 

                    {
                        chofer=choferDAO.buscarChofer(Cadena);

                       vchofer.getTxtNombre().setText(chofer.getNombre());
                        vchofer.getTxtDireccion().setText(chofer.getDireccion()); 
                        vchofer.getDchNacimiento().setDate(chofer.getFechaNacimiento());
                        vchofer.getTxtTelefono().setText(chofer.getTelefono());
                        vchofer.getCmbSexo().setSelectedItem(chofer.getSexo());
                        vchofer.getTxtCooperativa().setText(chofer.getCodCoop());


                        enableBotones(false);
                        vchofer.getBtnActualizar().setEnabled(true);
                        vchofer.getBtnBorrar().setEnabled(true);
                        vchofer.getBtnCancelar().setEnabled(true);
                        vchofer.getBtnSalir().setEnabled(true);


                        enableTxt(true);
                        vchofer.getTxtCedula().setEnabled(false);

                       } 

//                             else
//                       {
//
//                           enableBotones(false);
//                           vchofer.getBtnGuardar().setEnabled(true);
//                           vchofer.getBtnBorrar().setEnabled(true);
//                           enableTxt(true);
//                           vchofer.getTxtNombre().requestFocus();
//
//                       }



                    }

        
        
        if (e.getSource().equals(vchofer.getBtnGuardar()))
        {            
            
           //Validar Campos vacios
            
           if (vchofer.getTxtCedula().getText().trim().length()==0)
           {
                vchofer.mostrarMensaje("El campo cedula no puede estar vacio");
                vchofer.getTxtCedula().requestFocus();
                return;
           }
           
           if (vchofer.getTxtNombre().getText().trim().length()==0)
           {
               vchofer.mostrarMensaje("El campo nombre no puede estar vacio");
               vchofer.getTxtNombre().requestFocus();
               return;
           }
           
            enableTxt(false);
           if (vchofer.getTxtDireccion().getText().trim().length()==0)
           {
               vchofer.mostrarMensaje("El campo direccion no puede estar vacio");
               vchofer.getTxtDireccion().requestFocus();
               return;
           }
         
           if (vchofer.getDchNacimiento().getDate().toString().length()==0)
           {
               vchofer.mostrarMensaje("El campo Fecha de Nacimiento no puede estar vacio");
               vchofer.getDchNacimiento().requestFocus();
               return;
           }
           
           if (vchofer.getTxtTelefono().getText().trim().length()==0)
           {
               vchofer.mostrarMensaje("El campo Telefono no puede estar vacio");
               vchofer.getTxtTelefono().requestFocus();
               return;
           }
           
           if (vchofer.getCmbSexo().getSelectedIndex()==0)
           {
               vchofer.mostrarMensaje("Seleccione el Sexo del cliente");
               vchofer.getCmbSexo().requestFocus();
               return;
           }

          
          if (vchofer.getTxtCooperativa().getText().trim().length()==0)
           {
               vchofer.mostrarMensaje("El campo Cod. Cooperativa no puede estar vacio");
               vchofer.getTxtCooperativa().requestFocus();
               return;
           }
           
                        
            cedula = vchofer.getTxtCedula().getText();
            nombre = vchofer.getTxtNombre().getText();
            direccion = vchofer.getTxtDireccion().getText();
            fecha = vchofer.getDchNacimiento().getDate();
            telefono = vchofer.getTxtTelefono().getText();
            sexo = vchofer.getCmbSexo().getSelectedItem().toString();
            codCoop = vchofer.getTxtCooperativa().getText();
                  
            
           
            Chofer chofer = new Chofer(codCoop, cedula, nombre, direccion, telefono, fecha, sexo);
            choferDAO.registrarChofer(chofer);            
            CargarTablaChoferes();
            
            vchofer.mostrarMensaje("Se ha agregado un chofer nuevo");
            enableBotones(false);
            enableTxt(false);
            Limpiar();
            
            vchofer.getBtnBuscar().setEnabled(true);
            vchofer.getBtnNuevo().setEnabled(true);
            vchofer.getBtnCancelar().setEnabled(true);
            vchofer.getBtnSalir().setEnabled(true);
        
            vchofer.getTxtCedula().setEnabled(true);
            vchofer.getTxtCedula().requestFocus();
                
        }
         if(e.getSource().equals(vchofer.getBtnActualizar()))
        {
            actualizarChofer();
        }
        if(e.getSource().equals(vchofer.getBtnBorrar()))
        {
            eliminarChofer();
        }

        if(e.getSource().equals(vchofer.getBtnSalir()))
        {
            vchofer.dispose();
        }
      
    }
    
    
    
}
