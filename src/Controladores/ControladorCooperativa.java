/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;


import DAOS.DAOCooperativa;
import Modelos.Cooperativa;
import Vistas.VCooperativa;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;


public class ControladorCooperativa extends KeyAdapter implements ActionListener, KeyListener
{
    private VCooperativa vcooperativa;
    private DAOCooperativa cooperativaDAO;

    public ControladorCooperativa()
    {
        super();
        vcooperativa = new VCooperativa();
        cooperativaDAO = new DAOCooperativa();
        vcooperativa.agregarListener(this);
        vcooperativa.setLocationRelativeTo(null);
        vcooperativa.setVisible(true);
       
        enableBotones(false);
        
        vcooperativa.getBtnNuevo().setEnabled(true);
        vcooperativa.getBtnSalir().setEnabled(true);
        vcooperativa.getBtnBuscar().setEnabled(true);
        vcooperativa.getBtnCancelar().setEnabled(true);
        
        CargarTablaCooperativas();
        
        vcooperativa.getTxtRif().addKeyListener(new KeyAdapter()
    { 

        @Override
        public void keyTyped(KeyEvent e) 
        {     

            
            String Cadena=vcooperativa.getTxtRif().getText();
            if (Cadena.length()==10)
            {  
                e.consume();   
                return;
            } 
            
            
        }
       
    }
                 
                 
    );
        
          vcooperativa.getTxtNombre().addKeyListener(new KeyAdapter() 
       {
           @Override
          public void keyTyped(KeyEvent e) 
             {
               		if(Character.isDigit(e.getKeyChar())){ //validando que NO es numero
			e.consume(); //Desecha el caracter
		}
             }
       }
       );
          
          vcooperativa.getTxtTelefono().addKeyListener(new KeyAdapter() 
       {
           @Override
          public void keyTyped(KeyEvent e) 
             {
               		if(Character.isAlphabetic(e.getKeyChar())){ //validando que NO es numero
			e.consume(); //Desecha el caracter
		}
             }
       }
       );   
        
        
        
        
    }
    
    private void CargarTablaCooperativas()
   {
      
    int Fila;
    Cooperativa cooperativa=new Cooperativa();
    
    cooperativaDAO.getCooper().clear();
    cooperativaDAO.cargarCooperativa();
    vcooperativa.getTabCooperativas().removeAll();

    for (Fila=0;Fila<cooperativaDAO.getCooper().size();Fila++)
    {
       cooperativa=cooperativaDAO.getCooper().get(Fila);

       vcooperativa.getTabCooperativas().setValueAt(cooperativa.getRif(), Fila, 0);
       vcooperativa.getTabCooperativas().setValueAt(cooperativa.getNombre(), Fila, 1);
       vcooperativa.getTabCooperativas().setValueAt(cooperativa.getDireccion(), Fila, 2);
       vcooperativa.getTabCooperativas().setValueAt(cooperativa.getTelefono(), Fila, 3);

    }     
    
    
    }
    
     private void enableTxt(boolean Status)    
  {
    vcooperativa.getTxtRif().setEnabled(!Status);
    vcooperativa.getTxtNombre().setEnabled(Status); 
    vcooperativa.getTxtDireccion().setEnabled(Status);
    vcooperativa.getTxtTelefono().setEnabled(Status);   
  } 
    
    private void Limpiar()    
  {
    vcooperativa.getTxtRif().setText("");
    vcooperativa.getTxtNombre().setText(""); 
    vcooperativa.getTxtDireccion().setText("");
    vcooperativa.getTxtTelefono().setText("");

  }    
    
    private void enableBotones(boolean Status)
    {
         vcooperativa.getBtnGuardar().setEnabled(Status);
        vcooperativa.getBtnActualizar().setEnabled(Status);
        vcooperativa.getBtnBorrar().setEnabled(Status);
        vcooperativa.getBtnCancelar().setEnabled(Status);
        vcooperativa.getBtnNuevo().setEnabled(Status);
        vcooperativa.getBtnBuscar().setEnabled(Status);
    }

    
    @Override
    public void actionPerformed(ActionEvent e) 
    {
        
         if(e.getSource().equals(vcooperativa.getBtnNuevo()))
            {
                    
                
                enableBotones(false);
                vcooperativa.getBtnGuardar().setEnabled(true);
                vcooperativa.getBtnCancelar().setEnabled(true);
                
                enableTxt(true);
                Limpiar();
                vcooperativa.getTxtRif().setEnabled(true);
                vcooperativa.getTxtRif().requestFocus();

            
            }
        
        if (e.getSource().equals(vcooperativa.getBtnCancelar()))
        {
                enableBotones(false);
                enableTxt(false);
                
                Limpiar();
                
                vcooperativa.getBtnCancelar().setEnabled(true);
                vcooperativa.getBtnBuscar().setEnabled(true);
                vcooperativa.getBtnNuevo().setEnabled(true);
                vcooperativa.getBtnSalir().setEnabled(true);
                
                vcooperativa.getTxtRif().setEnabled(true);
                vcooperativa.getTxtRif().requestFocus();
        }
        
        
        if (e.getSource().equals(vcooperativa.getBtnBuscar()))
        {
                    String Cadena;
                    Cooperativa coop=new Cooperativa();
                    Cadena =vcooperativa.getTxtRif().getText();
                    vcooperativa.getTxtRif().setText(Cadena.toUpperCase());


                    if (Cadena.length() > 0) 

                    {
                      
                          coop=cooperativaDAO.buscarCooperativa(Cadena); 

                        vcooperativa.getTxtNombre().setText(coop.getNombre());
                       vcooperativa.getTxtDireccion().setText(coop.getDireccion()); 
                       vcooperativa.getTxtTelefono().setText(coop.getTelefono());

                           enableBotones(true);
                           vcooperativa.getBtnGuardar().setEnabled(false);
                           vcooperativa.getBtnNuevo().setEnabled(false);
                           enableTxt(true);
                           vcooperativa.getTxtRif().setEnabled(false);
                           
                          vcooperativa.mostrarMensaje("La cooperativa ya existe");
                          
                       
//
//                           enableBotones(false);
//                           vcooperativa.getBtnGuardar().setEnabled(true);
//                           vcooperativa.getBtnCancelar().setEnabled(true);
//                           enableTxt(true);
//                           vcooperativa.getTxtNombre().requestFocus();




                    }

        }
        
        if (e.getSource().equals(vcooperativa.getBtnGuardar()))
        {
            if(vcooperativa.getTxtRif().getText().trim().length() == 0)
            {
                vcooperativa.mostrarMensaje("El campo RIF no puede estar vacio");
                vcooperativa.getTxtRif().requestFocus();
                return;
            }
            
            if(vcooperativa.getTxtNombre().getText().trim().length() == 0)
            {
                vcooperativa.mostrarMensaje("El campo Nombre no puede estar vacio");
                vcooperativa.getTxtNombre().requestFocus();
                return;
            }
            
            if(vcooperativa.getTxtDireccion().getText().trim().length() == 0)
            {
                vcooperativa.mostrarMensaje("El campo direccion no puede estar vacio");
                vcooperativa.getTxtRif().requestFocus();
                return;
            }
            
            if(vcooperativa.getTxtTelefono().getText().trim().length() == 0)
            {
                vcooperativa.mostrarMensaje("El campo Telefono no puede estar vacio");
                vcooperativa.getTxtTelefono().requestFocus();
                return;
            }
            
            String rif = vcooperativa.getTxtRif().getText();
            String nombre = vcooperativa.getTxtNombre().getText();
            String direccion = vcooperativa.getTxtDireccion().getText();
            String telefono = vcooperativa.getTxtTelefono().getText();
            
            Cooperativa coop = new Cooperativa(rif,nombre,direccion,telefono);
            
            cooperativaDAO.registrarCooperativa(coop);
            CargarTablaCooperativas();
            vcooperativa.mostrarMensaje("Se ha agregado una nueva cooperativa");
            
            enableBotones(false);
            enableTxt(false);
            Limpiar();
            vcooperativa.getBtnBuscar().setEnabled(true);
            vcooperativa.getBtnNuevo().setEnabled(true);
            vcooperativa.getBtnCancelar().setEnabled(true);
            vcooperativa.getBtnSalir().setEnabled(true);

            vcooperativa.getTxtRif().setEnabled(true);
            vcooperativa.getTxtRif().requestFocus();
            
        }
        
          if(e.getSource().equals(vcooperativa.getBtnActualizar()))
        {

        actualizarCooperativa();
        
        }
           if(e.getSource().equals(vcooperativa.getBtnBorrar()))
        {

        eliminarCooperativa();
        
        }
           if(e.getSource().equals(vcooperativa.getBtnSalir()))
        {
            vcooperativa.dispose();
        }
        
    }
    
     public void actualizarCooperativa()
        {
            cooperativaDAO = new DAOCooperativa();
            cooperativaDAO.actualizarCooperativa(vcooperativa.getTxtRif().getText(),vcooperativa.getTxtNombre().getText(), vcooperativa.getTxtDireccion().getText(),vcooperativa.getTxtTelefono().getText());
            vcooperativa.mostrarMensaje("La cooperativa ha sido Actualizada exitosamente");
            Limpiar();
            enableTxt(false);
            vcooperativa.getTxtRif().setEnabled(true);
            vcooperativa.getTxtRif().requestFocus();
            vcooperativa.getBtnActualizar().setEnabled(false);
            vcooperativa.getBtnBorrar().setEnabled(false);
            vcooperativa.getBtnNuevo().setEnabled(true);
            CargarTablaCooperativas();
        }
     public void eliminarCooperativa()
        {
            cooperativaDAO = new DAOCooperativa();
            cooperativaDAO.eliminarCooperativa(vcooperativa.getTxtRif().getText());
            vcooperativa.mostrarMensaje("La cooperativa ha sido eliminada exitosamente");
            Limpiar();
            enableTxt(false);
            enableBotones(false);
            
            vcooperativa.getBtnBuscar().setEnabled(true);
            vcooperativa.getBtnNuevo().setEnabled(true);
            vcooperativa.getBtnSalir().setEnabled(true);
            vcooperativa.getBtnCancelar().setEnabled(true);
            vcooperativa.getTxtRif().setEnabled(true);
            vcooperativa.getTxtRif().requestFocus();
            
            CargarTablaCooperativas();
        }
    
    
}
